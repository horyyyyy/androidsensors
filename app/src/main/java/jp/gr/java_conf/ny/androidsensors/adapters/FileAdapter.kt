package jp.gr.java_conf.ny.androidsensors.adapters

import android.content.Context
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import jp.gr.java_conf.ny.androidsensors.R


class FileAdapter(private val context: Context) : RecyclerView.Adapter<FileAdapter.ViewHolder>() {

    private var files = context.fileList()

    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val textView = itemView.findViewById(R.id.textView) as TextView
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int)
            = LayoutInflater.from(parent.context).inflate(R.layout.text_view, parent, false).let(::ViewHolder)

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.textView.setOnLongClickListener(null)
        val file = files[position]
        holder.textView.text = file
        holder.textView.setOnLongClickListener {
            it.context.deleteFile(file)
            files = it.context.fileList()
            notifyItemRemoved(position)
            notifyItemRangeChanged(position, files.size)
            true
        }
    }

    override fun getItemCount() = files.size
}