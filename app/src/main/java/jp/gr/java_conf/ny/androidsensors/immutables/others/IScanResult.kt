package jp.gr.java_conf.ny.androidsensors.immutables.others


interface IScanResult {
    val BSSID: String
    val SSID: String
    val capabilities: String
    val centerFreq0: Int
    val centerFreq1: Int
    val channelWidth: Int
    val frequency: Int
    val is80211mcResponder: Boolean
    val isPasspointNetwork: Boolean
    val level: Int
    val operatorFriendlyName: CharSequence
    val timestamp: Long
    val venueName: CharSequence
}