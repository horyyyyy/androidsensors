package jp.gr.java_conf.ny.androidsensors.immutables.bluetooth

import android.os.ParcelUuid
import jp.gr.java_conf.ny.androidsensors.immutables.others.ICsv

data class ImmutableUUID(
        override val leastSignificantBits: Long =0,
        override val mostSignificantBits: Long =0
) : IUUID, ICsv {
    constructor(parcelUuid: ParcelUuid) : this(
            leastSignificantBits = parcelUuid.uuid.leastSignificantBits,
            mostSignificantBits = parcelUuid.uuid.mostSignificantBits
    )

    override fun toCsv() = "$leastSignificantBits,$mostSignificantBits"
}