package jp.gr.java_conf.ny.androidsensors.files

import android.content.Context
import jp.gr.java_conf.ny.androidsensors.immutables.bluetooth.ImmutableBluetoothDevice
import jp.gr.java_conf.ny.androidsensors.immutables.others.ICsv
import jp.gr.java_conf.ny.androidsensors.immutables.others.ImmutableBatteryInformation
import jp.gr.java_conf.ny.androidsensors.immutables.others.ImmutableLocation
import jp.gr.java_conf.ny.androidsensors.immutables.others.ImmutableScanResult
import jp.gr.java_conf.ny.androidsensors.immutables.sensors.ImmutableSensorEvent
import jp.gr.java_conf.ny.androidsensors.immutables.sensors.ImmutableTriggerEvent
import java.io.File
import java.io.InputStream
import java.io.OutputStream

enum class KryoCsvFile(private val clazz: Class<out ICsv>) : Copyable {
    IMMUTABLE_BLUETOOTH_DEVICE(ImmutableBluetoothDevice::class.java),
    IMMUTABLE_BATTERY_INFORMATION(ImmutableBatteryInformation::class.java),
    IMMUTABLE_LOCATION(ImmutableLocation::class.java),
    IMMUTABLE_SCAN_RESULT(ImmutableScanResult::class.java),
    IMMUTABLE_SENSOR_EVENT(ImmutableSensorEvent::class.java),
    IMMUTABLE_TRIGGER_EVENT(ImmutableTriggerEvent::class.java);

    override fun copy(from: InputStream, to: OutputStream) {
        ReadableFile(from, clazz).readAll().map(ICsv::toCsv).joinToString("\n").apply { to.writer().write(this) }
    }

    override fun copy(from: InputStream, directory: String, filename: String) = copy(from, File(directory, "$filename.csv").outputStream())
}