package jp.gr.java_conf.ny.androidsensors.immutables.bluetooth

import android.bluetooth.BluetoothDevice
import jp.gr.java_conf.ny.androidsensors.immutables.others.ICsv

data class ImmutableBluetoothDevice(
        override val address: String = "",
        override val bluetoothClass: ImmutableBluetoothClass = ImmutableBluetoothClass(),
        override val bondState: Int = 0,
        override val name: String = "",
        override val type: Int = 0,
        override val uuids: List<ImmutableUUID> = emptyList()
) : IBluetoothDevice, ICsv {
    constructor(bluetoothDevice: BluetoothDevice) : this(
            address = bluetoothDevice.address,
            bluetoothClass = ImmutableBluetoothClass(bluetoothDevice.bluetoothClass),
            bondState = bluetoothDevice.bondState,
            name = bluetoothDevice.name,
            type = bluetoothDevice.type,
            uuids = if (bluetoothDevice.uuids != null) bluetoothDevice.uuids.map(::ImmutableUUID) else emptyList()
    )

    override fun toCsv() = "$address,${bluetoothClass.toCsv()},$bondState,$name,$type,${uuids.map(ICsv::toCsv).joinToString(",")}"
}