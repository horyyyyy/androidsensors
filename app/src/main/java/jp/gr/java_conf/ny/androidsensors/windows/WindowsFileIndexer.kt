package jp.gr.java_conf.ny.androidsensors.windows

import java.io.File
import java.text.SimpleDateFormat
import java.util.*

private val csv by lazy { "C:\\Users\\Lion\\Desktop\\研究資料\\csv" }
private val format by lazy { SimpleDateFormat("yyyyMMdd", Locale.JAPAN) }

fun main(args: Array<String>) = test(csv)

fun test(pathname: String) {
    File(pathname).listFiles()?.filter(File::isFile)?.forEach {
        val timestamp = it.name.substringAfter('@').toLongOrNull() ?: return Unit

        val day = File(csv, format.format(Date(timestamp)))
        if (!day.exists()) day.mkdir()

        it.copyTo(File(day, it.name))
    }
}