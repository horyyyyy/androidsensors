package jp.gr.java_conf.ny.androidsensors.immutables.sensors

import android.hardware.SensorEvent
import jp.gr.java_conf.ny.androidsensors.immutables.others.ICsv

data class ImmutableSensorEvent(
        override val accuracy: Int = 0,
        override val sensor: ImmutableSensor = ImmutableSensor(),
        override val timestamp: Long = 0,
        override val values: List<Float> = emptyList()
) : ISensorEvent, ICsv {
    constructor(sensorEvent: SensorEvent) : this(
            sensorEvent.accuracy,
            ImmutableSensor(sensorEvent.sensor),
            sensorEvent.timestamp,
            sensorEvent.values.toList()
    )

    override fun toCsv() = "$accuracy,${sensor.toCsv()},$timestamp,${values.joinToString(",")}"
}