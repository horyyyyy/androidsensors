package jp.gr.java_conf.ny.androidsensors.immutables.others

// Kryo could not create classes have lazy init
interface ICsv {
    fun toCsv(): String
}