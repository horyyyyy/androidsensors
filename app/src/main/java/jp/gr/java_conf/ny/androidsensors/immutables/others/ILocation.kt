package jp.gr.java_conf.ny.androidsensors.immutables.others

interface ILocation {
    val accuracy: Float
    val altitude: Double
    val bearing: Float
    val elapsedRealtimeNanos: Long
    val isFromMockProvider: Boolean
    val latitude: Double
    val longitude: Double
    val provider: String
    val speed: Float
    val time: Long
    val hasAccuracy: Boolean
    val hasAltitude: Boolean
    val hasBearing: Boolean
    val hasSpeed: Boolean
}
