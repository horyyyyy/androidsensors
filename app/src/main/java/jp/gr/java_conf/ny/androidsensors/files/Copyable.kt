package jp.gr.java_conf.ny.androidsensors.files

import java.io.InputStream
import java.io.OutputStream

interface Copyable {
    fun copy(from: InputStream, to: OutputStream)
    fun copy(from: InputStream, directory: String, filename: String)
}