package jp.gr.java_conf.ny.androidsensors.adapters

import android.content.Context
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.TextView
import jp.gr.java_conf.ny.androidsensors.R
import java.math.BigDecimal
import java.math.BigInteger


class TimeAdapter : RecyclerView.Adapter<TimeAdapter.ViewHolder>() {

    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val button = itemView.findViewById(R.id.button) as Button
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int)
            = LayoutInflater.from(parent.context).inflate(R.layout.button, parent, false).let(::ViewHolder)

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.button.setOnClickListener(null)
        holder.button.setOnClickListener {
            val (currentTimeMillisTotal, nanoTimeTotal) = (1..100).map { it % 2 == 0 }
                    .map {
                        if (it) {
                            val currentTimeMillis = System.currentTimeMillis()
                            val nanoTime = System.nanoTime()
                            currentTimeMillis to nanoTime
                        } else {
                            val nanoTime = System.nanoTime()
                            val currentTimeMillis = System.currentTimeMillis()
                            currentTimeMillis to nanoTime
                        }
                    }
                    .map { (currentTimeMillis, nanoTime) -> BigInteger.valueOf(currentTimeMillis) to BigInteger.valueOf(nanoTime) }
                    .reduce { acc, pair -> acc.first + pair.first to acc.second + pair.second }

            val hundred = BigInteger.valueOf(100)
            holder.button.text = "${currentTimeMillisTotal / hundred}, ${nanoTimeTotal / hundred}"
        }
    }

    override fun getItemCount() = 1
}