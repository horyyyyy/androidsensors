package jp.gr.java_conf.ny.androidsensors.files

import java.io.Closeable

interface Writable<in T> : Closeable {
    fun write(t: T)
}