package jp.gr.java_conf.ny.androidsensors.adapters.switchables

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import jp.gr.java_conf.ny.androidsensors.files.Extension
import jp.gr.java_conf.ny.androidsensors.files.Writable
import jp.gr.java_conf.ny.androidsensors.files.WritableFile
import jp.gr.java_conf.ny.androidsensors.immutables.others.ImmutableBatteryInformation
import jp.gr.java_conf.ny.androidsensors.immutables.others.ImmutableScanResult
import java.io.Closeable


class BatteryManagerAdapter(val context: Context) : AbstractManagerAdapter<String>() {
    override val items by lazy { listOf("Battery") }

    override fun String.toText() = this

    override fun register(item: String): Closeable {
        val receiver = MyBroadcastReceiver(context)
        context.registerReceiver(receiver, IntentFilter(Intent.ACTION_BATTERY_CHANGED))

        return Closeable {
            context.unregisterReceiver(receiver)
            receiver.close()
        }
    }

    private class MyBroadcastReceiver(private val file: Writable<ImmutableBatteryInformation>) : BroadcastReceiver(), Closeable by file {
        constructor(context: Context) : this(Extension.IMMUTABLE_BATTERY_INFORMATION_KRYO.writer("Battery", context) as WritableFile<ImmutableBatteryInformation>)

        override fun onReceive(context: Context, intent: Intent) = file.write(ImmutableBatteryInformation(intent))
    }


}