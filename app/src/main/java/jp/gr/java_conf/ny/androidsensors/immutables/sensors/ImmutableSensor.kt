package jp.gr.java_conf.ny.androidsensors.immutables.sensors

import android.hardware.Sensor
import jp.gr.java_conf.ny.androidsensors.immutables.others.ICsv

// Kryo could not create classes have lazy init
// Kryo require no-arg constructor
data class ImmutableSensor(
        override val fifoMaxEventCount: Int = 0,
        override val fifoReservedEventCount: Int = 0,
        // override val id: Int = 0,
        // override val isAdditionalInfoSupported: Boolean = false,
        // override val isDynamicSensor: Boolean = false,
        override val isWakeUpSensor: Boolean = false,
        override val maxDelay: Int = 0,
        override val maximumRange: Float = 0F,
        override val minDelay: Int = 0,
        override val name: String = "",
        override val power: Float = 0F,
        override val reportingMode: Int = 0,
        override val resolution: Float = 0F,
        override val stringType: String = "",
        override val type: Int = 0,
        override val vendor: String = "",
        override val version: Int = 0
) : ISensor, ICsv {
    constructor(sensor: Sensor) : this(
            fifoMaxEventCount = sensor.fifoMaxEventCount,
            fifoReservedEventCount = sensor.fifoReservedEventCount,
            // id = sensor.id,
            // isAdditionalInfoSupported = sensor.isAdditionalInfoSupported,
            // isDynamicSensor = sensor.isDynamicSensor,
            isWakeUpSensor = sensor.isWakeUpSensor,
            maxDelay = sensor.maxDelay,
            maximumRange = sensor.maximumRange,
            minDelay = sensor.minDelay,
            name = sensor.name,
            power = sensor.power,
            reportingMode = sensor.reportingMode,
            resolution = sensor.resolution,
            stringType = sensor.stringType,
            type = sensor.type,
            vendor = sensor.vendor,
            version = sensor.version
    )

    override fun toCsv() = "$fifoMaxEventCount,$fifoReservedEventCount,$isWakeUpSensor,$maxDelay,$maximumRange,$minDelay,$name,$power,$reportingMode,$resolution,$stringType,$type,$vendor,$version"
}
