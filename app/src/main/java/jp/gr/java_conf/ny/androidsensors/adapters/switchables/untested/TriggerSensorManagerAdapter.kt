package jp.gr.java_conf.ny.androidsensors.adapters.switchables.untested

import android.content.Context
import android.hardware.Sensor
import android.hardware.SensorManager
import android.hardware.TriggerEvent
import android.hardware.TriggerEventListener
import jp.gr.java_conf.ny.androidsensors.adapters.switchables.AbstractManagerAdapter
import jp.gr.java_conf.ny.androidsensors.files.Extension
import jp.gr.java_conf.ny.androidsensors.files.Writable
import jp.gr.java_conf.ny.androidsensors.files.WritableFile
import jp.gr.java_conf.ny.androidsensors.immutables.sensors.ImmutableTriggerEvent
import java.io.Closeable

class TriggerSensorManagerAdapter(private val context: Context) : AbstractManagerAdapter<Sensor>() {

    private val sensorManager by lazy { context.getSystemService(Context.SENSOR_SERVICE) as SensorManager }
    override val items: List<Sensor> by lazy { sensorManager.getSensorList(Sensor.TYPE_SIGNIFICANT_MOTION) }

    override fun android.hardware.Sensor.toText() = name ?: ""

    override fun register(item: Sensor): Closeable {
        val listener = MySensorEventListener(item.name, context)
        sensorManager.requestTriggerSensor(listener, item)
        return Closeable {
            sensorManager.cancelTriggerSensor(listener, item)
            listener.close()
        }
    }

    private class MySensorEventListener(val file: Writable<ImmutableTriggerEvent>) : TriggerEventListener(), Closeable by file {
        override fun onTrigger(event: TriggerEvent?) {
            if (event != null) {
                file.write(ImmutableTriggerEvent(event))
            }
        }

        constructor(sensorName: String, context: Context) : this(Extension.IMMUTABLE_TRIGGER_EVENT_KRYO.writer(sensorName, context) as WritableFile<ImmutableTriggerEvent>)
    }

}