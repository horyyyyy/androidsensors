package jp.gr.java_conf.ny.androidsensors.immutables.sensors

import android.hardware.TriggerEvent
import jp.gr.java_conf.ny.androidsensors.immutables.others.ICsv

class ImmutableTriggerEvent(
        override val sensor: ImmutableSensor = ImmutableSensor(),
        override val timestamp: Long = 0,
        override val values: List<Float> = emptyList()
) : ITriggerEvent, ICsv {
    constructor(event: TriggerEvent) : this(
            sensor = ImmutableSensor(event.sensor),
            timestamp = event.timestamp,
            values = event.values.toList()
    )

    override fun toCsv() = "${sensor.toCsv()},$timestamp,${values.joinToString(",")}"
}