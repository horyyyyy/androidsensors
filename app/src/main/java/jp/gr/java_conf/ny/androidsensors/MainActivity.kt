package jp.gr.java_conf.ny.androidsensors

import android.os.Bundle
import android.support.v7.widget.RecyclerView
import android.support.wearable.activity.WearableActivity
import android.support.wearable.view.WearableRecyclerView
import jp.gr.java_conf.ny.androidsensors.adapters.FileAdapter
import jp.gr.java_conf.ny.androidsensors.adapters.TimeAdapter
import jp.gr.java_conf.ny.androidsensors.adapters.switchables.*
import jp.gr.java_conf.ny.androidsensors.adapters.switchables.untested.*
import net.cattaka.android.adaptertoolbox.thirdparty.MergeRecyclerAdapter
import java.io.Closeable

class MainActivity : WearableActivity() {

    private lateinit var adapter: MergeRecyclerAdapter<*>

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setAmbientEnabled()
        setContentView(R.layout.wearable_recycler_view)

        this.adapter = MergeRecyclerAdapter<RecyclerView.Adapter<*>>(this).apply {
            addAdapter(TimeAdapter())
            addAdapter(LocationManagerAdapter(this@MainActivity))
            addAdapter(SensorManagerAdapter(this@MainActivity))
            addAdapter(TriggerSensorManagerAdapter(this@MainActivity))
            addAdapter(MediaRecorderAdapter(this@MainActivity))
            // onにしなくても ファイルが作成される？
            addAdapter(BatteryManagerAdapter(this@MainActivity))
            addAdapter(WifiManagerAdapter(this@MainActivity))
            addAdapter(BluetoothManagerAdapter(this@MainActivity))
            addAdapter(FileAdapter(this@MainActivity))
        }

        //アプリ落ちた時close
        val savedUncaughtExceptionHandler = Thread.getDefaultUncaughtExceptionHandler()
        Thread.setDefaultUncaughtExceptionHandler { thread, ex ->
            (0..(adapter.subAdapterCount - 1)).map(adapter::getSubAdapter).forEach { if (it is Closeable) it.close() }
            savedUncaughtExceptionHandler.uncaughtException(thread, ex)
        }

        val recyclerView = findViewById(R.id.wearable_list) as WearableRecyclerView
        recyclerView.adapter = this.adapter
    }

    override fun onPause() {
        super.onPause()
        (0..(adapter.subAdapterCount - 1)).map(adapter::getSubAdapter).forEach { if (it is Closeable) it.close() }
    }
}
