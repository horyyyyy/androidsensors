package jp.gr.java_conf.ny.androidsensors.files

import android.content.Context
import jp.gr.java_conf.ny.androidsensors.immutables.bluetooth.ImmutableBluetoothDevice
import jp.gr.java_conf.ny.androidsensors.immutables.others.ICsv
import jp.gr.java_conf.ny.androidsensors.immutables.others.ImmutableBatteryInformation
import jp.gr.java_conf.ny.androidsensors.immutables.others.ImmutableLocation
import jp.gr.java_conf.ny.androidsensors.immutables.others.ImmutableScanResult
import jp.gr.java_conf.ny.androidsensors.immutables.sensors.ImmutableSensorEvent
import jp.gr.java_conf.ny.androidsensors.immutables.sensors.ImmutableTriggerEvent

enum class Extension(private val suffix: String, val clazz: Class<out ICsv>, val copyable: Copyable) {
    IMMUTABLE_BLUETOOTH_DEVICE_KRYO("ibd_kryo", ImmutableBluetoothDevice::class.java, KryoCsvFile.IMMUTABLE_BLUETOOTH_DEVICE),
    IMMUTABLE_BATTERY_INFORMATION_KRYO("ibi_kryo", ImmutableBatteryInformation::class.java, KryoCsvFile.IMMUTABLE_BATTERY_INFORMATION),
    IMMUTABLE_LOCATION_KRYO("il_kryo", ImmutableLocation::class.java, KryoCsvFile.IMMUTABLE_LOCATION),
    IMMUTABLE_SCAN_RESULT_KRYO("isr_kryo", ImmutableScanResult::class.java, KryoCsvFile.IMMUTABLE_SCAN_RESULT),
    IMMUTABLE_SENSOR_EVENT_KRYO("ise_kryo", ImmutableSensorEvent::class.java, KryoCsvFile.IMMUTABLE_SENSOR_EVENT),
    IMMUTABLE_TRIGGER_EVENT_KRYO("ite_kryo", ImmutableTriggerEvent::class.java, KryoCsvFile.IMMUTABLE_TRIGGER_EVENT),
    THREE_GPP("3gpp", ICsv::class.java, RawFile.THREE_GPP);

    fun writer(name: String, context: Context) = WritableFile("${name.replace(' ', '_')}@${System.currentTimeMillis()}.$suffix", context, clazz)

    companion object {
        private val reverseLookup = values().associate { it.suffix to it }
        fun of(string: String) = reverseLookup[string]
    }

    override fun toString() = suffix
}