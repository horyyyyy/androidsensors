package jp.gr.java_conf.ny.androidsensors.immutables.others

import android.location.Location

data class ImmutableLocation(
        override val accuracy: Float = 0F,
        override val altitude: Double = 0.0,
        override val bearing: Float = 0F,
        override val elapsedRealtimeNanos: Long = 0,
        override val isFromMockProvider: Boolean = false,
        override val latitude: Double = 0.0,
        override val longitude: Double = 0.0,
        override val provider: String = "",
        override val speed: Float = 0F,
        override val time: Long = 0,
        override val hasAccuracy: Boolean = false,
        override val hasAltitude: Boolean = false,
        override val hasBearing: Boolean = false,
        override val hasSpeed: Boolean = false
) : ILocation, ICsv {
    constructor(location: Location) : this(
            accuracy = location.accuracy,
            altitude = location.altitude,
            bearing = location.bearing,
            elapsedRealtimeNanos = location.elapsedRealtimeNanos,
            isFromMockProvider = location.isFromMockProvider,
            latitude = location.latitude,
            longitude = location.longitude,
            provider = location.provider,
            speed = location.speed,
            time = location.time,
            hasAccuracy = location.hasAccuracy(),
            hasAltitude = location.hasAltitude(),
            hasBearing = location.hasBearing(),
            hasSpeed = location.hasSpeed()
    )

    override fun toCsv() = "$accuracy,$altitude,$bearing,$elapsedRealtimeNanos,$isFromMockProvider,$latitude,$longitude,$provider,$speed,$time,$hasAccuracy,$hasAltitude,$hasBearing,$hasSpeed"
}
