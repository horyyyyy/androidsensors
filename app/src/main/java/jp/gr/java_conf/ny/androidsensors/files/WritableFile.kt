package jp.gr.java_conf.ny.androidsensors.files

import android.content.Context
import com.esotericsoftware.kryo.Kryo
import com.esotericsoftware.kryo.io.Output
import java.io.Closeable


private val kryo by lazy { Kryo() }

class WritableFile<T : Any>(private val output: Output, type: Class<T>) : Writable<T>, Closeable by output {
    constructor(name: String, context: Context, type: Class<T>) : this(Output(context.openFileOutput(name.replace(' ', '_'), Context.MODE_PRIVATE)), type)

    init {
        kryo.register(type)
    }

    override fun write(t: T) = kryo.writeObject(output, t)
}