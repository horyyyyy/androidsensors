package jp.gr.java_conf.ny.androidsensors.files

import java.io.Closeable

interface Readable<out T> : Closeable {
    fun read(): T
}