package jp.gr.java_conf.ny.androidsensors.immutables.bluetooth

interface IUUID {
    val leastSignificantBits: Long
    val mostSignificantBits: Long
}