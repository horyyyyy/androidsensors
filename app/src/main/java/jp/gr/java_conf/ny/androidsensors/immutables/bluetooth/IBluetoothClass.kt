package jp.gr.java_conf.ny.androidsensors.immutables.bluetooth

interface IBluetoothClass {
    val deviceClass: Int
    val majorDeviceClass: Int
}