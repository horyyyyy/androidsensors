package jp.gr.java_conf.ny.androidsensors.immutables.bluetooth

interface IBluetoothDevice {
    val address: String
    val bluetoothClass: IBluetoothClass
    val bondState: Int
    val name: String
    val type: Int
    val uuids: List<IUUID>
}