package jp.gr.java_conf.ny.androidsensors.immutables.others

interface IBatteryInformation {
    val status: Int
    val health: Int
    val present: Boolean
    val level: Int
    val scale: Int
    val iconSmall: Int
    val plugged: Int
    val voltage: Int
    val temperature: Int
    val technology: String
}