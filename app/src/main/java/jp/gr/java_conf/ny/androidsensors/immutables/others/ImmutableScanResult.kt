package jp.gr.java_conf.ny.androidsensors.immutables.others

import android.net.wifi.ScanResult

data class ImmutableScanResult(
        override val BSSID: String = "",
        override val SSID: String = "",
        override val capabilities: String = "",
        override val centerFreq0: Int = 0,
        override val centerFreq1: Int = 0,
        override val channelWidth: Int = 0,
        override val frequency: Int = 0,
        override val is80211mcResponder: Boolean = false,
        override val isPasspointNetwork: Boolean = false,
        override val level: Int = 0,
        override val operatorFriendlyName: String = "",
        override val timestamp: Long = 0,
        override val venueName: String = ""
) : IScanResult, ICsv {
    constructor(scanResult: ScanResult) : this(
            BSSID = scanResult.BSSID,
            SSID = scanResult.SSID,
            capabilities = scanResult.capabilities,
            centerFreq0 = scanResult.centerFreq0,
            centerFreq1 = scanResult.centerFreq1,
            channelWidth = scanResult.channelWidth,
            frequency = scanResult.frequency,
            is80211mcResponder = scanResult.is80211mcResponder,
            isPasspointNetwork = scanResult.isPasspointNetwork,
            level = scanResult.level,
            operatorFriendlyName = scanResult.operatorFriendlyName.toString(),
            timestamp = scanResult.timestamp,
            venueName = scanResult.venueName.toString()
    )

    override fun toCsv() = "$BSSID,$SSID,$capabilities,$centerFreq0,$centerFreq1,$channelWidth,$frequency,$is80211mcResponder,$isPasspointNetwork,$level,$operatorFriendlyName,$timestamp,$venueName"
}