package jp.gr.java_conf.ny.androidsensors.adapters.switchables.untested

import android.content.Context
import android.location.Location
import android.location.LocationListener
import android.location.LocationManager
import android.os.Bundle
import jp.gr.java_conf.ny.androidsensors.adapters.switchables.AbstractManagerAdapter
import jp.gr.java_conf.ny.androidsensors.files.Extension
import jp.gr.java_conf.ny.androidsensors.files.Writable
import jp.gr.java_conf.ny.androidsensors.files.WritableFile
import jp.gr.java_conf.ny.androidsensors.immutables.others.ImmutableLocation
import java.io.Closeable


class LocationManagerAdapter(val context: Context) : AbstractManagerAdapter<String>() {

    private val locationManager by lazy { context.getSystemService(Context.LOCATION_SERVICE) as LocationManager }
    override val items: List<String> by lazy { locationManager.getProviders(true) }

    override fun String.toText() = this

    override fun register(item: String): Closeable {
        val listener = MyLocationListener(item, context)
        locationManager.requestLocationUpdates(item, 0, 0F, listener)
        return Closeable {
            locationManager.removeUpdates(listener)
            listener.close()
        }
    }

    private class MyLocationListener(private val file: Writable<ImmutableLocation>) : LocationListener, Closeable by file {
        constructor(provider: String, context: Context) : this(Extension.IMMUTABLE_LOCATION_KRYO.writer(provider, context) as WritableFile<ImmutableLocation>)

        override fun onLocationChanged(location: Location) = file.write(ImmutableLocation(location))
        override fun onStatusChanged(provider: String?, status: Int, extras: Bundle?) = Unit
        override fun onProviderEnabled(provider: String?) = Unit
        override fun onProviderDisabled(provider: String?) = Unit
    }
}