package jp.gr.java_conf.ny.androidsensors.immutables.sensors

interface ITriggerEvent {
    val sensor: ISensor
    val timestamp: Long
    val values: List<Float>
}