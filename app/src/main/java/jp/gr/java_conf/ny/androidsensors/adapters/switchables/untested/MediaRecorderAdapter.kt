package jp.gr.java_conf.ny.androidsensors.adapters.switchables.untested

import android.content.Context
import android.media.MediaRecorder
import jp.gr.java_conf.ny.androidsensors.adapters.switchables.AbstractManagerAdapter
import jp.gr.java_conf.ny.androidsensors.files.Extension
import java.io.Closeable


class MediaRecorderAdapter(private val context: Context) : AbstractManagerAdapter<String>() {
    override val items by lazy { listOf("MediaRecorder") }

    override fun String.toText() = this

    override fun register(item: String): Closeable {
        val mediaRecorder = MediaRecorder()
        mediaRecorder.setAudioSource(MediaRecorder.AudioSource.MIC)
        mediaRecorder.setOutputFormat(MediaRecorder.OutputFormat.THREE_GPP)
        println("${context.filesDir.path}\\${item.replace(' ', '_')}@${System.currentTimeMillis()}.${Extension.THREE_GPP}")
        mediaRecorder.setOutputFile("${context.filesDir.path}/${item.replace(' ', '_')}@${System.currentTimeMillis()}.${Extension.THREE_GPP}")
        mediaRecorder.setAudioEncoder(MediaRecorder.AudioEncoder.AMR_NB)

        mediaRecorder.prepare()
        mediaRecorder.start()

        return Closeable {
            mediaRecorder.stop()
            mediaRecorder.release()
        }
    }

}