package jp.gr.java_conf.ny.androidsensors.windows

import android.graphics.Path
import android.os.Build
import jp.gr.java_conf.ny.androidsensors.files.Extension
import java.io.*
import java.nio.channels.Channels


private val adb by lazy { """C:\Users\Lion\AppData\Local\Android\sdk\platform-tools\adb""" }
private val `package` by lazy { "jp.gr.java_conf.ny.androidsensors" }
private val csv by lazy { "C:\\Users\\Lion\\Desktop\\研究資料\\csv" }


fun adb(command: String) = ProcessBuilder(adb, "exec-out", command).start().inputStream

val sdkInt by lazy { adb("getprop ro.build.version.sdk").bufferedReader().use(BufferedReader::readLine).toInt() }

val ls by lazy { "ls ${if (sdkInt >= Build.VERSION_CODES.N_MR1) "-1" else ""}" }

val filenames by lazy { adb("run-as $`package` $ls files").bufferedReader().use(BufferedReader::readLines).filter(String::isNotBlank) }

fun main(args: Array<String>) {
    filenames.map { it to it.split('.') }
            .forEach {
                (fullname, splitted) ->
                val (filename, extension) = splitted
                println("fullname is $fullname")
                println("filename is $filename")
                println("extension is $extension")
                val inputStream = adb("run-as $`package` cat \"files/$fullname\"")
                Extension.of(extension)!!.copyable.copy(inputStream, csv, filename)
            }
}