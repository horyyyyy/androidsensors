package jp.gr.java_conf.ny.androidsensors.adapters.switchables.untested

import android.bluetooth.BluetoothAdapter
import android.bluetooth.BluetoothDevice
import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import jp.gr.java_conf.ny.androidsensors.adapters.switchables.AbstractManagerAdapter
import jp.gr.java_conf.ny.androidsensors.files.Extension
import jp.gr.java_conf.ny.androidsensors.files.WritableFile
import jp.gr.java_conf.ny.androidsensors.immutables.bluetooth.ImmutableBluetoothDevice
import java.io.Closeable

class BluetoothManagerAdapter(private val context: Context) : AbstractManagerAdapter<String>() {
    private val bluetoothAdapter = BluetoothAdapter.getDefaultAdapter()
    override val items by lazy { if (bluetoothAdapter == null || !bluetoothAdapter.isEnabled) emptyList() else listOf("Bluetooth") }

    override fun String.toText() = this

    override fun register(item: String): Closeable {
        val file = Extension.IMMUTABLE_BLUETOOTH_DEVICE_KRYO.writer("Bluetooth", context) as WritableFile<ImmutableBluetoothDevice>
        val receiver = object : BroadcastReceiver() {
            override fun onReceive(context: Context, intent: Intent) {
                val action = intent.action
                if (BluetoothDevice.ACTION_FOUND == action) {
                    val device = intent.getParcelableExtra<BluetoothDevice>(BluetoothDevice.EXTRA_DEVICE)
                    file.write(ImmutableBluetoothDevice(device))
                }
            }
        }

        context.registerReceiver(receiver, IntentFilter(BluetoothDevice.ACTION_FOUND))
        bluetoothAdapter.startDiscovery()

        return Closeable {
            bluetoothAdapter.cancelDiscovery()
            context.unregisterReceiver(receiver)
            file.close()
        }
    }
}