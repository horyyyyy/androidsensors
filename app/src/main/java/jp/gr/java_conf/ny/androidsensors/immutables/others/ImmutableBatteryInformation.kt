package jp.gr.java_conf.ny.androidsensors.immutables.others

import android.content.Intent
import android.os.BatteryManager

data class ImmutableBatteryInformation(
        override val status: Int = 0,
        override val health: Int = 0,
        override val present: Boolean = false,
        override val level: Int = 0,
        override val scale: Int = 0,
        override val iconSmall: Int = 0,
        override val plugged: Int = 0,
        override val voltage: Int = 0,
        override val temperature: Int = 0,
        override val technology: String = ""
) : IBatteryInformation, ICsv {
    constructor(intent: Intent) : this(
            intent.getIntExtra(BatteryManager.EXTRA_STATUS, -1),
            intent.getIntExtra(BatteryManager.EXTRA_HEALTH, -1),
            intent.getBooleanExtra(BatteryManager.EXTRA_PRESENT, false),
            intent.getIntExtra(BatteryManager.EXTRA_LEVEL, -1),
            intent.getIntExtra(BatteryManager.EXTRA_SCALE, -1),
            intent.getIntExtra(BatteryManager.EXTRA_ICON_SMALL, -1),
            intent.getIntExtra(BatteryManager.EXTRA_PLUGGED, -1),
            intent.getIntExtra(BatteryManager.EXTRA_VOLTAGE, -1),
            intent.getIntExtra(BatteryManager.EXTRA_TEMPERATURE, -1),
            intent.getStringExtra(BatteryManager.EXTRA_TECHNOLOGY))

    override fun toCsv() = "$status,$health,$present,$level,$scale,$iconSmall,$plugged,$voltage,$temperature,$technology"
}