package jp.gr.java_conf.ny.androidsensors.immutables.sensors

interface ISensor {
    val fifoMaxEventCount: Int
    val fifoReservedEventCount: Int
    // val id: Int
    // val isAdditionalInfoSupported: Boolean
    // val isDynamicSensor: Boolean
    val isWakeUpSensor: Boolean
    val maxDelay: Int
    val maximumRange: Float
    val minDelay: Int
    val name: String
    val power: Float
    val reportingMode: Int
    val resolution: Float
    val stringType: String
    val type: Int
    val vendor: String
    val version: Int
}
