package jp.gr.java_conf.ny.androidsensors.immutables.bluetooth

import android.bluetooth.BluetoothClass
import jp.gr.java_conf.ny.androidsensors.immutables.others.ICsv

data class ImmutableBluetoothClass(
        override val deviceClass: Int = 0,
        override val majorDeviceClass: Int = 0
) : IBluetoothClass, ICsv {
    constructor(bluetoothClass: BluetoothClass) : this(
            deviceClass = bluetoothClass.deviceClass,
            majorDeviceClass = bluetoothClass.majorDeviceClass
    )

    override fun toCsv() = "$deviceClass,$majorDeviceClass"
}