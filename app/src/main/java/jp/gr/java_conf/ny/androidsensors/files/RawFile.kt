package jp.gr.java_conf.ny.androidsensors.files

import java.io.File
import java.io.InputStream
import java.io.OutputStream

enum class RawFile(private val suffix: String) : Copyable {
    THREE_GPP("3gpp");

    override fun copy(from: InputStream, to: OutputStream) {
        from.copyTo(to)
    }

    override fun copy(from: InputStream, directory: String, filename: String) = copy(from, File(directory, "$filename.$suffix").outputStream())
}