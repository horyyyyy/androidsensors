package jp.gr.java_conf.ny.androidsensors.immutables.sensors

interface ISensorEvent {
    val accuracy: Int
    val sensor: ISensor
    val timestamp: Long
    val values: List<Float>
}
