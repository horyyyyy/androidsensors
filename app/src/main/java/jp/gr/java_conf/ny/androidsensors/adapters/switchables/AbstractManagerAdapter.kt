package jp.gr.java_conf.ny.androidsensors.adapters.switchables

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Switch
import jp.gr.java_conf.ny.androidsensors.R
import java.io.Closeable

abstract class AbstractManagerAdapter<T> : RecyclerView.Adapter<AbstractManagerAdapter.ViewHolder>(), Closeable {
    protected abstract val items: List<T>
    protected abstract fun T.toText(): String
    protected abstract fun register(item: T): Closeable

    override fun close() = closeables.values.forEach(Closeable::close)
    override fun getItemCount() = items.size

    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val sensorSwitch = itemView.findViewById(R.id.sensorSwitch) as Switch
    }

    private val closeables = mutableMapOf<T, Closeable>()
    private val checks = mutableMapOf<T, Boolean>()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val itemView = LayoutInflater.from(parent.context).inflate(R.layout.list_item_switch, parent, false)
        return ViewHolder(itemView)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        // setOnCheckedChangeListener は setChecked より先に呼ばないと 挙動が変
        // null set で防げるっぽいから積極的に代入しておく
        holder.sensorSwitch.setOnCheckedChangeListener(null)

        val item = items[position]

        holder.sensorSwitch.text = item.toText()
        holder.sensorSwitch.isChecked = checks[item] ?: false

        holder.sensorSwitch.setOnCheckedChangeListener { _, b ->
            checks[item] = b
            if (b) {
                closeables[item] = register(item)
            } else {
                closeables.remove(item)?.close()
            }
        }
    }
}