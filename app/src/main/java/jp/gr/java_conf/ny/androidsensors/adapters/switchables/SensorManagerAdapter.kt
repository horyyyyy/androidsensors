package jp.gr.java_conf.ny.androidsensors.adapters.switchables

import android.content.Context
import android.hardware.Sensor
import android.hardware.SensorEvent
import android.hardware.SensorEventListener
import android.hardware.SensorManager
import jp.gr.java_conf.ny.androidsensors.files.Extension
import jp.gr.java_conf.ny.androidsensors.files.Writable
import jp.gr.java_conf.ny.androidsensors.files.WritableFile
import jp.gr.java_conf.ny.androidsensors.immutables.sensors.ImmutableSensorEvent
import jp.gr.java_conf.ny.androidsensors.immutables.sensors.ImmutableTriggerEvent
import java.io.Closeable


class SensorManagerAdapter(private val context: Context) : AbstractManagerAdapter<Sensor>() {

    private val sensorManager by lazy { context.getSystemService(Context.SENSOR_SERVICE) as SensorManager }
    // Any motion Sensor が排除できない
    override val items by lazy { sensorManager.getSensorList(Sensor.TYPE_ALL).filter { it.type != Sensor.TYPE_SIGNIFICANT_MOTION } }

    override fun Sensor.toText() = name ?: ""

    override fun register(item: Sensor): Closeable {
        val listener = MySensorEventListener(item.name, context)
        sensorManager.registerListener(listener, item, SensorManager.SENSOR_DELAY_FASTEST)
        return java.io.Closeable {
            sensorManager.unregisterListener(listener)
            listener.close()
        }
    }

    private class MySensorEventListener(private val file: Writable<ImmutableSensorEvent>) : SensorEventListener, Closeable by file {
        constructor(sensorName: String, context: Context) : this(Extension.IMMUTABLE_SENSOR_EVENT_KRYO.writer(sensorName, context) as WritableFile<ImmutableSensorEvent>)

        override fun onAccuracyChanged(sensor: Sensor, accuracy: Int) = Unit
        override fun onSensorChanged(event: SensorEvent) = file.write(ImmutableSensorEvent(event))
    }

}