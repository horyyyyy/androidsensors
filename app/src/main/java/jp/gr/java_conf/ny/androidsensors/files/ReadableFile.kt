package jp.gr.java_conf.ny.androidsensors.files

import com.esotericsoftware.kryo.Kryo
import com.esotericsoftware.kryo.io.Input
import java.io.Closeable
import java.io.InputStream


private val kryo by lazy { Kryo() }

class ReadableFile<T : Any>(private val input: Input, private val type: Class<T>) : Readable<T>, Closeable by input {
    constructor(inputStream: InputStream, type: Class<T>) : this(Input(inputStream), type)

     init {
        kryo.register(type)
     }

    fun readAll(): List<T> {
        val list = ArrayList<T>()
        while (!input.eof()) {
            list.add(kryo.readObject(input, type))
        }
        return list
    }

    override fun read(): T = kryo.readObject(input, type)
}