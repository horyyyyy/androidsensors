package jp.gr.java_conf.ny.androidsensors.adapters.switchables.untested

import android.content.Context
import android.net.wifi.WifiManager
import jp.gr.java_conf.ny.androidsensors.adapters.switchables.AbstractManagerAdapter
import jp.gr.java_conf.ny.androidsensors.files.Extension
import jp.gr.java_conf.ny.androidsensors.files.WritableFile
import jp.gr.java_conf.ny.androidsensors.immutables.others.ImmutableScanResult
import java.io.Closeable
import java.util.concurrent.Executors
import java.util.concurrent.TimeUnit


class WifiManagerAdapter(private val context: Context, private val samplingPeriodNs: Long = 10000000000L) : AbstractManagerAdapter<String>() {
    override val items by lazy { listOf("Wifi") }

    private val manager = context.applicationContext.getSystemService(Context.WIFI_SERVICE) as WifiManager

    override fun String.toText() = this

    override fun register(item: String): Closeable {
        val executor = Executors.newSingleThreadScheduledExecutor()
        val file = Extension.IMMUTABLE_SCAN_RESULT_KRYO.writer("Wifi", context) as WritableFile<ImmutableScanResult>
        executor.scheduleAtFixedRate({
            val scanResults = manager.scanResults.map(::ImmutableScanResult)
            scanResults.forEach(file::write)
        }, 0, samplingPeriodNs, TimeUnit.NANOSECONDS)

        return Closeable {
            executor.shutdownNow()
            file.close()
        }
    }

}